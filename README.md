# Chess challenge

My scalac chess challenge. I've experimented a bit with different approaches and as a result there are two different implementations.
One can be found in `FilteringBacktrackingBoard.scala` and the other in `MarkingBacktrackingBoard.scala` and both are parallelized.

### Running challenge

```
sbt
run 7x7 -k 2 -q 2 -b 2 -n 1 --print false
...

Used 8 workers with filtering strategy
Found 3063828 solutions
Solved in 2079ms

```

To use another implementation pass `--marking-strategy true` and you can change number of workers with `--workers` option.

For example:

```
run 7x7 -k 2 -q 2 -b 2 -n 1 --print false --marking-strategy true --workers 1
...

Used 1 worker with marking strategy
Found 3063828 solutions
Solved in 4097ms
```

### Enjoy!
