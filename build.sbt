import Dependencies._

name := "chess-challenge"

version := "1.1"

scalaVersion := "2.12.7"

mainClass in(Compile, run) := Some("chess.Main")
mainClass in(Compile, packageBin) := Some("chess.Main")


libraryDependencies += scalaTest % Test
libraryDependencies += scopt
