import sbt._

object Dependencies {
  lazy val scalaTest = "org.scalatest" %% "scalatest" % "3.0.5"
  lazy val scopt = "com.github.scopt" %% "scopt" % "3.7.0"
}
