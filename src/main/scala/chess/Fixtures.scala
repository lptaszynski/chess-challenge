package chess

import chess.solver.Challenge

object Fixtures {

  val empty = Challenge(1, 1)

  val example1 = Challenge(3, 3, kings = 2, rooks = 1)

  val example2 = Challenge(4, 4, rooks = 2, knights = 4)

  val example3 = Challenge(7, 7, kings = 2, queens = 2, bishops = 2, knights = 1)

  val example3Smaller = Challenge(6, 6, kings = 2, queens = 2, bishops = 2, knights = 1)

  def nQueens(n: Int): Challenge = Challenge(n, n, queens = n)

  val twoKings = Challenge(3, 3, kings = 2)

  val eachPieceOnce = Challenge(3, 6, kings = 1, queens = 1, rooks = 1, bishops = 1, knights = 1)

  val fourQueensAndOther = Challenge(8, 8, rooks = 2, bishops = 2, queens = 4)

  val largeWithTwoKings = Challenge(20, 20, kings = 2)

  val largeWithOneKing = Challenge(100, 100, kings = 1)

  val twoKingsTwoBishops = Challenge(5, 5, kings = 2, bishops = 2)

}
