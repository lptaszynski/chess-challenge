package chess

import chess.solver.Challenge

case class Input(challenge: Challenge = Challenge(1, 1),
                 workers: Int = Utilities.getCores,
                 markingStrategy: Boolean = false,
                 print: Boolean = true)
