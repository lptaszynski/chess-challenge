package chess

import chess.Utilities._
import chess.solver._


object Main extends App {

  val optInput = if (args.nonEmpty) parser.parse(args, Input()) else Some(Input(Fixtures.example3))

  optInput.foreach { input =>
    val boardFactory = if (input.markingStrategy) new MarkingBacktrackingBoard(_) else new FilteringBacktrackingBoard(_)
    val solver = new ParallelSolver(boardFactory, input.workers)
    findSolutions(solver, input)
  }

  private def findSolutions(solver: ParallelSolver, input: Input): Unit = {

    val (solutions, solvingTime) = withExecutionTime(solver.solve(input.challenge))

    var summary = s"Used ${solver.workers} ${if (solver.workers == 1) "worker" else "workers"}"
    summary += s" with ${if (input.markingStrategy) "marking" else "filtering"} strategy"
    summary += s"\nFound ${solutions.size} solutions\nSolved in ${solvingTime}ms"

    if (input.print) {
      val (_, printingTime) = withExecutionTime(printSolutions(solutions, input.challenge))
      summary += s"\nPrinted in ${printingTime}ms"
    }

    println(summary)
  }

  private def printSolutions(solutions: Seq[Solution], challenge: Challenge): Unit = {
    val separator = "*" * challenge.M + "\n"
    val groupSize = math.max(1, math.pow(1024, 2).toInt / challenge.MN)
    val stringChunks = solutions.grouped(groupSize).map { chunk =>
      chunk.map(_.representation).mkString(separator, separator, separator)
    }
    stringChunks.foreach(println)
  }

  private def parser: scopt.OptionParser[Input] =
    new scopt.OptionParser[Input]("Chess Challenge") {

      private def definePawnOpt(char: Char, name: String)(action: (Int, Challenge) => Challenge) =
        opt[Int](char, name)
          .validate(pawns => if (pawns > 0) success else failure(s"Number of $name must be greater than zero"))
          .action((pawns: Int, input: Input) => input.copy(challenge = action(pawns, input.challenge)))

      arg[String]("dimensions").required()
        .text("Dimensions of the board")
        .validate { value =>
          val split = value.split("x|X")
          if (split.length == 2 && split(0).toInt > 0 && split(1).toInt > 0) {
            success
          } else {
            failure("Argument should match pattern MxN where M and N are greater than zero")
          }
        }
        .action { (value, c) =>
          val dims = value.split("x|X")
          c.copy(challenge = c.challenge.copy(M = dims(0).toInt, N = dims(1).toInt))
        }

      opt[Int]('w', "workers")
        .validate(value => if (value > 0) success else failure("Number of workers must be greater than zero"))
        .action((value, c) => c.copy(workers = value))

      opt[Boolean]("marking-strategy")
        .text("Use different implementation of backtracking based on marking threats on the board")
        .action((value, c) => c.copy(markingStrategy = value))

      opt[Boolean]("print")
        .text("Decide whether to print all solutions")
        .action((value, c) => c.copy(print = value))

      definePawnOpt('k', "kings")((pawns, c) => c.copy(kings = pawns))
      definePawnOpt('q', "queens")((pawns, c) => c.copy(queens = pawns))
      definePawnOpt('r', "rooks")((pawns, c) => c.copy(rooks = pawns))
      definePawnOpt('b', "bishops")((pawns, c) => c.copy(bishops = pawns))
      definePawnOpt('n', "knights")((pawns, c) => c.copy(knights = pawns))
    }

}

