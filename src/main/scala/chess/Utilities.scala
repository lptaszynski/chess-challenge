package chess

import scala.annotation.tailrec
import scala.collection.mutable

object Utilities {

  implicit class Crossable[X](list: List[List[X]]) {
    def crossJoin: List[List[X]] =
      list match {
        case xs :: Nil => xs map (List(_))
        case x :: xs => for {
          i <- x
          j <- xs.crossJoin
        } yield List(i) ++ j
        case Nil => Nil
      }
  }

  def getCores: Int = Runtime.getRuntime.availableProcessors()

  def withExecutionTime[R](block: => R): (R, Long) = {
    val t0 = System.currentTimeMillis()
    val result = block // call-by-name
    val t1 = System.currentTimeMillis()
    (result, t1 - t0)
  }

  def factorials(n: Int, maxLength: Int = Int.MaxValue): List[Int] = {
    var results = mutable.ArrayBuffer[Int]()
    var divisor = 2
    var quotient = n
    val untilDivisor = math.sqrt(quotient).toInt
    while (divisor <= untilDivisor && results.length < maxLength - 1) {
      if (quotient % divisor == 0) {
        quotient = quotient / divisor
        results += divisor
      } else {
        divisor += 1
      }
    }

    if (quotient > 1) results += quotient

    results.toList
  }

  implicit class OrderedPattern[T](partialPattern: List[T]) {

    @tailrec
    private def isOrderedSubsetRec(partialPattern: List[T], pattern: List[T]): Boolean = {
      if (partialPattern.isEmpty) {
        true
      } else if (pattern.isEmpty) {
        false
      } else {
        val patternHead :: patternTail = pattern
        val partialPatternHead :: partialPatternTail = partialPattern
        if (patternHead == partialPatternHead) {
          isOrderedSubsetRec(partialPatternTail, patternTail)
        } else {
          isOrderedSubsetRec(partialPattern, patternTail)
        }
      }
    }

    def isOrderedSubsetOf(pattern: List[T]): Boolean = {
      isOrderedSubsetRec(partialPattern, pattern)
    }

  }

}
