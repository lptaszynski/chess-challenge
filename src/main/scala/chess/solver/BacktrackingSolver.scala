package chess.solver

class BacktrackingSolver(override val boardFactory: Challenge => SolverBoard) extends Solver {

  override def solve(challenge: Challenge): Seq[Solution] = {
    val board = boardFactory(challenge)
    board.generateSolutions()
  }

}
