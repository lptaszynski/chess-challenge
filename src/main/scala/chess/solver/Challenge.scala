package chess.solver

import scala.collection.mutable

case class Challenge(M: Int, N: Int, kings: Int = 0, queens: Int = 0, bishops: Int = 0, rooks: Int = 0, knights: Int = 0) {

  val MN: Int = M * N

  val pieces: List[Piece] = {
    List.fill(queens)(Queen) ++
      List.fill(rooks)(Rook) ++
      List.fill(bishops)(Bishop) ++
      List.fill(kings)(King) ++
      List.fill(knights)(Knight)
  }

  val mRange: Range = 0 until M
  val nRange: Range = 0 until N

  val allPositions: List[Position] = (for (n <- nRange; m <- mRange) yield Position(m, n)).toList

  val permutations: List[Permutation] = {
    pieces.permutations.map(Permutation(_, generateSymmetric = false)).toList
  }

  val permutationsWithoutSymmetric: List[Permutation] = {
    val set: mutable.Set[Permutation] = mutable.Set()

    permutations.foreach { permutation =>
      val pieces = permutation.pieces
      val reversedPieces = pieces.reverse

      if (reversedPieces == pieces) {
        set += Permutation(pieces, generateSymmetric = false)
      } else {
        val reversedPermutation = Permutation(reversedPieces, generateSymmetric = true)
        if (!set.contains(reversedPermutation)) {
          set += Permutation(pieces, generateSymmetric = true)
        }
      }
    }
    set.toList
  }
}
