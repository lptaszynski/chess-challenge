package chess.solver

sealed trait Field extends Threat {

  def representation: Char

  def isEmpty: Boolean

  def nonEmpty: Boolean

}

case object EmptyField extends Field {
  override val representation: Char = ' '
  override val isEmpty: Boolean = true
  override val nonEmpty: Boolean = false
}

sealed trait Piece extends Field {
  override val isEmpty: Boolean = false
  override val nonEmpty: Boolean = true
}

case object King extends Piece with KingThreat {
  override val representation: Char = 'K'
}

case object Queen extends Piece with StraightThreat with DiagonalThreat {
  override val representation: Char = 'Q'
}

case object Bishop extends Piece with DiagonalThreat {
  override val representation: Char = 'B'
}

case object Rook extends Piece with StraightThreat {
  override val representation: Char = 'R'
}

case object Knight extends Piece with KnightThreat {
  override val representation: Char = 'N'
}
