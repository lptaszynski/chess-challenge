package chess.solver

import scala.annotation.tailrec
import scala.collection.mutable

class FilteringBacktrackingBoard(val challenge: Challenge) extends SolverBoard {

  override def generateSolutions(partitions: List[IndexPartition] = Nil): Vector[Solution] = {
    val result = mutable.ArrayBuffer[Solution]()

    val filledPartitions = partitions ++ List.fill(challenge.pieces.length - partitions.length)(AllIndicesPartition)

    challenge.permutationsWithoutSymmetric.foreach { permutation =>
      result ++= generateSolutionsRec(
        challenge.pieces, Nil, filledPartitions, challenge.allPositions, challenge.allPositions, permutation, Nil)
    }

    result.toVector
  }

  @tailrec
  private def generateSolutionsRec(piecesLeft: List[Piece],
                                   piecesPlaced: List[(Position, Piece)],
                                   partitions: List[IndexPartition],
                                   positionsToCheck: List[Position],
                                   safePositions: List[Position],
                                   permutation: Permutation,
                                   result: List[Solution]): List[Solution] = {
    if (piecesLeft.isEmpty) {
      val solution = Solution(challenge.M, challenge.N, piecesPlaced)
      if (permutation.generateSymmetric) {
        solution :: solution.symmetric :: result
      } else {
        solution :: result
      }
    } else {
      if (positionsToCheck.nonEmpty) {
        val positionsHead :: positionsTail = positionsToCheck
        val partitionsHead :: partitionsTail = partitions

        if (partitionsHead.inPartition(positionToIndex(positionsHead))) {
          val piecesHead :: piecesTail = piecesLeft

          val isSafe = piecesPlaced.forall(pair => !piecesHead.threatens(positionsHead, pair._1))

          if (isSafe) {
            val placedHead = positionsHead -> piecesHead
            val piecesPlacedWithHead = placedHead :: piecesPlaced
            val orderedPieces = piecesPlacedWithHead.sortBy(_._1).map(_._2)
            if (permutation.contains(orderedPieces)) {
              val filteredSafePositions = safePositions.filterNot(piecesHead.threatens(positionsHead, _))

              val sameAsPrevious = piecesPlaced.nonEmpty && piecesPlaced.head._2 == piecesHead

              val nextPositionsToCheck = if (sameAsPrevious) {
                filteredSafePositions.filter(_ > positionsHead) // To eliminate duplicated solutions
              } else {
                filteredSafePositions
              }
              generateSolutionsRec(
                piecesLeft = piecesTail,
                piecesPlaced = piecesPlacedWithHead,
                partitions = partitionsTail,
                positionsToCheck = nextPositionsToCheck,
                safePositions = filteredSafePositions,
                permutation = permutation,
                result = result)
            } else {
              result
            }
          } else {
            result
          }
        } else {
          generateSolutionsRec(
            piecesLeft = piecesLeft,
            piecesPlaced = piecesPlaced,
            partitions = partitions,
            positionsToCheck = positionsTail,
            safePositions = safePositions,
            permutation = permutation,
            result = result)
        }
      } else {
        result
      }
    }
  }

  private def positionToIndex(position: Position): Int = position.n * challenge.M + position.m

}
