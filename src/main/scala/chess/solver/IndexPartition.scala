package chess.solver

sealed trait IndexPartition {

  def inPartition(index: => Int): Boolean

}

case object AllIndicesPartition extends IndexPartition {

  override def inPartition(index: => Int): Boolean = true

}

case class DivRemPartition(@specialized div: Int, @specialized rem: Int) extends IndexPartition {

  override def inPartition(index: => Int): Boolean = {
    index % div == rem
  }

}

object IndexPartition {

  def forDivisor(n: Int): List[DivRemPartition] = {
    (0 until n).map(DivRemPartition(n, _)).toList
  }

}
