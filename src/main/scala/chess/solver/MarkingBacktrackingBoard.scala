package chess.solver

import scala.collection.mutable

class MarkingBacktrackingBoard(override val challenge: Challenge) extends SolverBoard {

  final val M: Int = challenge.M
  final val N: Int = challenge.N
  final val MN: Int = M * N

  val mRange: Range = 0 until M
  val nRange: Range = 0 until N

  val indices: Range = 0 until MN

  val pieces: List[Piece] = challenge.pieces

  // Threats markers
  val fieldsThreats: Array[Int] = Array.fill(MN)(0)
  val isRowSafe: Array[Boolean] = Array.fill(N)(true)
  val isColumnSafe: Array[Boolean] = Array.fill(M)(true)
  val isFirstDiagonalSafe: Array[Boolean] = Array.fill(M + N)(true)
  val isSecondDiagonalSafe: Array[Boolean] = Array.fill(M + N)(true)

  // Occupations markers
  val occupation: Array[Field] = Array.fill(MN)(EmptyField)
  val rowsOccupations: Array[Int] = Array.fill(N)(0)
  val columnsOccupations: Array[Int] = Array.fill(M)(0)
  val firstDiagonalsOccupations: Array[Int] = Array.fill(M + N)(0)
  val secondDiagonalsOccupations: Array[Int] = Array.fill(M + N)(0)

  // Precomputed arrays of indices - it's faster this way
  val mArray: Array[Int] = indices.map(indexToM).toArray
  val nArray: Array[Int] = indices.map(indexToN).toArray
  val firstDiagonalArray: Array[Int] = indices.map(idx => firstDiagonal(mArray(idx), nArray(idx))).toArray
  val secondDiagonalArray: Array[Int] = indices.map(idx => secondDiagonal(mArray(idx), nArray(idx))).toArray
  val positions: Array[Position] = indices.map(idx => Position(indexToM(idx), indexToN(idx))).toArray

  // Pieces configuration
  var piecesArray: Array[Piece] = pieces.toArray
  val pieceSameAsPrevious: Array[Boolean] = pieces.indices.map { index =>
    if (index > 0) piecesArray(index - 1) == piecesArray(index) else false
  }.toArray

  var piecesStack: List[(Position, Piece)] = Nil

  override def generateSolutions(partition: List[IndexPartition] = Nil): Vector[Solution] = {
    if (pieces.isEmpty) return Vector()

    val results: mutable.ArrayBuffer[Solution] = mutable.ArrayBuffer()

    val MN = this.MN

    piecesStack = Nil

    val partitionsArray: Array[IndexPartition] =
      partition.toArray ++ Array.fill(piecesArray.length - partition.length)(AllIndicesPartition)
    val numberOfPieces = piecesArray.length

    var index = 0
    var piecesPlaced = 0

    @inline def backtrack(): Unit = {
      piecesPlaced -= 1
      val (position, piece) = piecesStack.head
      val indexToTake = positionToIndex(position)
      piecesStack = piecesStack.tail
      takePiece(indexToTake)
      index = indexToTake + 1
    }

    var inProgress = true
    while (inProgress) {
      if (piecesPlaced == numberOfPieces) {
        results += Solution(M, N, piecesStack)
        backtrack()
      } else if (index < MN) {
        val partition = partitionsArray(piecesPlaced)
        if (partition.inPartition(index)) {
          val piece = piecesArray(piecesPlaced)
          if (canPieceBePlaced(index, piece)) {
            placePiece(index, piece)
            piecesStack ::= (positions(index), piece)
            piecesPlaced += 1
            if (piecesPlaced < numberOfPieces && pieceSameAsPrevious(piecesPlaced)) index += 1 else index = 0
          } else index += 1
        } else index += 1
      } else if (piecesPlaced > 0) {
        backtrack()
      } else {
        inProgress = false
      }
    }

    results.toVector
  }

  def placePiece(index: Int, piece: Piece): Unit = {
    val m = mArray(index)
    val n = nArray(index)
    val firstDiagonalIndex = firstDiagonalArray(index)
    val secondDiagonalIndex = secondDiagonalArray(index)

    occupation(index) = piece
    rowsOccupations(n) += 1
    columnsOccupations(m) += 1
    firstDiagonalsOccupations(firstDiagonalIndex) += 1
    secondDiagonalsOccupations(secondDiagonalIndex) += 1

    if (piece.straightThreat) {
      isRowSafe(n) = false
      isColumnSafe(m) = false
    }

    if (piece.diagonalThreat) {
      isFirstDiagonalSafe(firstDiagonalIndex) = false
      isSecondDiagonalSafe(secondDiagonalIndex) = false
    }

    if (piece.fieldThreat) manipulateThreatsAround(m, n, piece.relativePositions, 1)
  }

  def takePiece(index: Int): Unit = {
    val piece = occupation(index)
    val m = mArray(index)
    val n = nArray(index)
    val firstDiagonalIndex = firstDiagonalArray(index)
    val secondDiagonalIndex = secondDiagonalArray(index)
    occupation(index) = EmptyField
    rowsOccupations(n) -= 1
    columnsOccupations(m) -= 1
    firstDiagonalsOccupations(firstDiagonalIndex) -= 1
    secondDiagonalsOccupations(secondDiagonalIndex) -= 1

    if (piece.straightThreat) {
      isRowSafe(n) = true
      isColumnSafe(m) = true
    }

    if (piece.diagonalThreat) {
      isFirstDiagonalSafe(firstDiagonalIndex) = true
      isSecondDiagonalSafe(secondDiagonalIndex) = true
    }

    if (piece.fieldThreat) manipulateThreatsAround(m, n, piece.relativePositions, -1)
  }

  def canPieceBePlaced(index: Int, piece: Piece): Boolean = {
    val m = mArray(index)
    val n = nArray(index)
    isFieldSafe(m, n, index) && isPieceNotThreatening(m, n, index, piece) && occupation(index).isEmpty
  }

  def isFieldSafe(m: Int, n: Int, index: Int): Boolean = {
    isRowSafe(n) &&
      isColumnSafe(m) &&
      isFirstDiagonalSafe(firstDiagonalArray(index)) &&
      isSecondDiagonalSafe(secondDiagonalArray(index)) &&
      fieldsThreats(index) == 0
  }

  def isPieceNotThreatening(m: Int, n: Int, index: Int, piece: Piece): Boolean = {
    (if (piece.straightThreat) {
      rowsOccupations(n) == 0 && columnsOccupations(m) == 0
    } else true) &&
      (if (piece.diagonalThreat) {
        firstDiagonalsOccupations(firstDiagonalArray(index)) == 0 && secondDiagonalsOccupations(secondDiagonalArray(index)) == 0
      } else true) &&
      (if (piece.fieldThreat) {
        areEmptyFieldsAround(m, n, piece.relativePositions)
      } else true)
  }

  def areEmptyFieldsAround(m: Int, n: Int, moves: Seq[(Int, Int)]): Boolean = {
    moves.forall { case (mDiff, nDiff) =>
      val mToCheck = m + mDiff
      val nToCheck = n + nDiff
      val index = positionToIndex(mToCheck, nToCheck)
      if (isWithinBoard(mToCheck, nToCheck)) occupation(index).isEmpty else true
    }
  }

  def manipulateThreatsAround(m: Int, n: Int, moves: Seq[(Int, Int)], toAdd: Int): Unit = {
    moves.foreach { case (mDiff, nDiff) =>
      val mToCheck = m + mDiff
      val nToCheck = n + nDiff
      if (isWithinBoard(mToCheck, nToCheck)) fieldsThreats(positionToIndex(mToCheck, nToCheck)) += toAdd
    }
  }

  def isWithinBoard(m: Int, n: Int): Boolean = m >= 0 && m < M && n >= 0 && n < N

  final def firstDiagonal(m: Int, n: Int): Int = M - 1 - m + n

  final def secondDiagonal(m: Int, n: Int): Int = m + n

  final def positionToIndex(position: Position): Int = positionToIndex(position.m, position.n)

  final def positionToIndex(m: Int, n: Int): Int = n * M + m

  final def indexToM(index: Int): Int = index % M

  final def indexToN(index: Int): Int = index / M

}
