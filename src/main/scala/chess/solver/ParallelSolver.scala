package chess.solver

import chess.Utilities._
import chess._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

class ParallelSolver(override val boardFactory: Challenge => SolverBoard,
                     val workers: Int = getCores) extends Solver {

  override def solve(challenge: Challenge): Seq[Solution] = {

    val partitions = partitionsPerWorker(challenge)

    val futures = partitions.map { partition =>
      Future {
        val board = new MarkingBacktrackingBoard(challenge)
        board.generateSolutions(partition)
      }
    }

    Await.result(Future.sequence(futures), 1 day).reduceLeft(_ ++ _)
  }

  def partitionsPerWorker(challenge: Challenge): List[List[DivRemPartition]] = {

    val divisors = Utilities.factorials(workers, maxLength = challenge.pieces.length)

    val partitions = if (divisors.nonEmpty) {
      divisors.map(n => IndexPartition.forDivisor(n)).crossJoin
    } else List(Nil)

    partitions
  }

}
