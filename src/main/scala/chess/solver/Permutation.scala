package chess.solver

import chess.Utilities.OrderedPattern

case class Permutation(pieces: List[Piece], generateSymmetric: Boolean) {

  def reverse: Permutation = Permutation(pieces.reverse, generateSymmetric)

  def contains(otherPieces: List[Piece]): Boolean = {
    otherPieces.isOrderedSubsetOf(pieces)
  }

}
