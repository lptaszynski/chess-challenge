package chess.solver

case class Position(@specialized m: Int, @specialized n: Int) extends Ordered[Position] {
  override def compare(that: Position): Int = {
    val nDiff = n - that.n
    if (nDiff != 0) nDiff else m - that.m
  }
}
