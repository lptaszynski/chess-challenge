package chess.solver

case class Puzzle(permutations: List[Piece], partition: List[DivRemPartition], generateSymmetricSolutions: Boolean)
