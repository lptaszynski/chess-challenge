package chess.solver

case class Solution(@specialized M: Int, @specialized N: Int, pieces: List[(Position, Piece)] = Nil) {

  def representation: String = {
    val stringBuilder = new StringBuilder((" " * M + "\n") * N)

    pieces.foreach { case (position, piece) =>
      val index = positionToIndex(position)
      stringBuilder.update(index + index / M, piece.representation)
    }

    stringBuilder.mkString
  }

  def updated(m: Int, n: Int)(field: Field): Solution = {
    val position = Position(m, n)
    field match {
      case piece: Piece =>
        val updatedPieces = pieces.toMap.updated(position, piece).toList
        copy(pieces = updatedPieces)
      case EmptyField =>
        val updatedPieces = (pieces.toMap - position).toList
        copy(pieces = updatedPieces)
    }
  }

  def symmetric: Solution = {
    val symmetricPieces = pieces.map { case (position, piece) =>
      val m = M - 1 - position.m
      val n = N - 1 - position.n
      (Position(m, n), piece)
    }
    Solution(M, N, symmetricPieces)
  }

  private def positionToIndex(position: Position): Int = position.n * M + position.m

}
