package chess.solver

trait Solver {

  def boardFactory: Challenge => SolverBoard

  def solve(challenge: Challenge): Seq[Solution]

  def getBoard(challenge: Challenge): SolverBoard = boardFactory(challenge)

}
