package chess.solver

trait SolverBoard {

  def challenge: Challenge

  def generateSolutions(partitions: List[IndexPartition] = Nil): Seq[Solution]

}
