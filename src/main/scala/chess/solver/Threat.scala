package chess.solver

abstract class Threat {

  val straightThreat: Boolean = false
  val diagonalThreat: Boolean = false
  val fieldThreat: Boolean = false

  val relativePositions: Seq[(Int, Int)] = Seq()

  def threatens(thisM: Int, thisN: Int, otherM: Int, otherN: Int): Boolean = false

  def threatens(thisPosition: Position, otherPosition: Position): Boolean =
    threatens(thisPosition.m, thisPosition.n, otherPosition.m, otherPosition.n)
}

trait StraightThreat extends Threat {

  override val straightThreat: Boolean = true

  override def threatens(thisM: Int, thisN: Int, otherM: Int, otherN: Int): Boolean = {
    super.threatens(thisM, thisN, otherM, otherN) || (thisM == otherM) || (thisN == otherN)
  }
}

trait DiagonalThreat extends Threat {

  override val diagonalThreat: Boolean = true

  override def threatens(thisM: Int, thisN: Int, otherM: Int, otherN: Int): Boolean = {
    super.threatens(thisM, thisN, otherM, otherN) || math.abs(thisM - otherM) == math.abs(thisN - otherN)
  }
}

trait FieldThreat extends Threat {

  override val fieldThreat: Boolean = true

}

trait KingThreat extends FieldThreat {

  override def threatens(thisM: Int, thisN: Int, otherM: Int, otherN: Int): Boolean = {
    math.abs(thisM - otherM) <= 1 && math.abs(thisN - otherN) <= 1
  }

  override val relativePositions: Seq[(Int, Int)] =
    for (mDiff <- -1 to 1; nDiff <- -1 to 1; if mDiff != 0 || nDiff != 0) yield (mDiff, nDiff)
}

trait KnightThreat extends FieldThreat {

  override def threatens(thisM: Int, thisN: Int, otherM: Int, otherN: Int): Boolean = {
    (math.abs(thisM - otherM) == 2 && math.abs(thisN - otherN) == 1) ||
      (math.abs(thisM - otherM) == 1 && math.abs(thisN - otherN) == 2)
  }

  override val relativePositions: Seq[(Int, Int)] =
    (for (mDiff <- Seq(-2, 2); nDiff <- Seq(-1, 1)) yield (mDiff, nDiff)) ++
      (for (mDiff <- Seq(-1, 1); nDiff <- Seq(-2, 2)) yield (mDiff, nDiff))

}