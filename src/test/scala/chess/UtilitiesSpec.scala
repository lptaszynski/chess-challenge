package chess

import org.scalatest._

class UtilitiesSpec extends FlatSpec with Matchers {

  "The factorials method" should "be correct" in {
    import Utilities.factorials

    factorials(1) shouldBe List()
    factorials(2) shouldBe List(2)
    factorials(3) shouldBe List(3)
    factorials(4) shouldBe List(2, 2)
    factorials(6) shouldBe List(2, 3)
    factorials(7) shouldBe List(7)
    factorials(8) shouldBe List(2, 2, 2)

    factorials(6, maxLength = 1) shouldBe List(6)
    factorials(6, maxLength = 2) shouldBe List(2, 3)
    factorials(8, maxLength = 2) shouldBe List(2, 4)

  }

  "Crossable" should "cross join properly" in {

    import Utilities.Crossable

    List(List(), List()).crossJoin shouldBe List()
    (Nil: List[List[Int]]).crossJoin shouldBe List()

    List(List(1, 2), List(7, 8, 9)).crossJoin shouldBe
      List(List(1, 7), List(1, 8), List(1, 9), List(2, 7), List(2, 8), List(2, 9))

  }

  "OrderedPattern" should "implement correct isOrderSubsetOf method" in {

    import Utilities.OrderedPattern

    val pattern1 = List(1, 2, 1)

    Nil.isOrderedSubsetOf(pattern1) shouldBe true
    pattern1.isOrderedSubsetOf(pattern1) shouldBe true
    List(1).isOrderedSubsetOf(pattern1) shouldBe true
    List(1,2).isOrderedSubsetOf(pattern1) shouldBe true
    List(2,1).isOrderedSubsetOf(pattern1) shouldBe true
    List(1,1).isOrderedSubsetOf(pattern1) shouldBe true
    List(1,2,1,2).isOrderedSubsetOf(pattern1) shouldBe false
    List(2,1,1).isOrderedSubsetOf(pattern1) shouldBe false

    val pattern2 = List(1, 2, 3, 1, 2, 3, 4)

    List(1,2,3,4).isOrderedSubsetOf(pattern2) shouldBe true

  }

}
