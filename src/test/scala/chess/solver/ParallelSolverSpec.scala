package chess.solver

import chess.Fixtures
import chess.Fixtures._
import org.scalactic.Equality
import org.scalatest._

class ParallelSolverSpec extends FlatSpec with Matchers {

  implicit val solutionEq: Equality[Solution] = new Equality[Solution] {
    def areEqual(a: Solution, b: Any): Boolean =
      b match {
        case b: Solution => a.M == b.M && a.N == b.N && a.pieces.sortBy(_._1) == b.pieces.sortBy(_._1)
        case _ => false
      }
  }

  val boardFactory = new FilteringBacktrackingBoard(_)

  "The single threaded backtracking solver" should "find proper solutions for given examples" in {

    val solver = new ParallelSolver(boardFactory, workers = 1)

    solver.solve(Fixtures.empty).length shouldBe 0

    solver.solve(example1).length shouldBe 4

    solver.solve(example2).length shouldBe 8

    solver.solve(nQueens(4)).length shouldBe 2

    solver.solve(nQueens(5)).length shouldBe 10

    solver.solve(twoKings).length shouldBe 16

    solver.solve(nQueens(8)).length shouldBe 92

  }


  "The parallel backtracking solver" should "partition pieces permutations" in {
    import Fixtures._

    new ParallelSolver(boardFactory, workers = 2).partitionsPerWorker(nQueens(4)) shouldBe
      List(
        List(DivRemPartition(2, 0)),
        List(DivRemPartition(2, 1))
      )

    new ParallelSolver(boardFactory, workers = 3).partitionsPerWorker(nQueens(4)) shouldBe
      List(
        List(DivRemPartition(3, 0)),
        List(DivRemPartition(3, 1)),
        List(DivRemPartition(3, 2))
      )

    new ParallelSolver(boardFactory, workers = 4).partitionsPerWorker(nQueens(4)) shouldBe
      List(
        List(DivRemPartition(2, 0), DivRemPartition(2, 0)),
        List(DivRemPartition(2, 0), DivRemPartition(2, 1)),
        List(DivRemPartition(2, 1), DivRemPartition(2, 0)),
        List(DivRemPartition(2, 1), DivRemPartition(2, 1))
      )

    new ParallelSolver(boardFactory, workers = 4).partitionsPerWorker(largeWithOneKing) shouldBe
      List(
        List(DivRemPartition(4, 0)),
        List(DivRemPartition(4, 1)),
        List(DivRemPartition(4, 2)),
        List(DivRemPartition(4, 3))
      )
  }

  "The parallel backtracking solver" should "find proper solutions for given examples" in {
    import Fixtures._

    val testedSolver = new ParallelSolver(boardFactory, workers = 8)
    val assumedValidSolver = new ParallelSolver(boardFactory, workers = 1)

    def compare(challenge: Challenge): Unit =
      testedSolver.solve(challenge) should contain theSameElementsAs assumedValidSolver.solve(challenge)

    compare(Fixtures.empty)
    compare(example1)
    compare(example2)
    compare(nQueens(4))
    compare(nQueens(5))
    compare(nQueens(8))
    compare(twoKings)
    compare(eachPieceOnce)
    compare(largeWithTwoKings)
    compare(largeWithOneKing)
    compare(twoKingsTwoBishops)
    compare(example3Smaller)
  }

  "The parallel backtracking solver using marking strategy" should "find proper solutions for given examples" in {
    import Fixtures._

    val testedSolver = new ParallelSolver(new MarkingBacktrackingBoard(_), workers = 8)
    val assumedValidSolver = new ParallelSolver(new FilteringBacktrackingBoard(_), workers = 8)

    def compare(challenge: Challenge): Unit =
      testedSolver.solve(challenge) should contain theSameElementsAs assumedValidSolver.solve(challenge)

    compare(Fixtures.empty)
    compare(example1)
    compare(example2)
    compare(nQueens(4))
    compare(nQueens(5))
    compare(nQueens(8))
    compare(twoKings)
    compare(eachPieceOnce)
    compare(largeWithTwoKings)
    compare(largeWithOneKing)
    compare(twoKingsTwoBishops)
    compare(example3Smaller)
  }


}
