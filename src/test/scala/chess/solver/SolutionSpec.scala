package chess.solver

import org.scalatest._

class SolutionSpec extends FlatSpec with Matchers {

  "The solution" should "have proper toString method" in {
    Solution(2, 2).representation shouldBe "  \n  \n"
    Solution(2, 2).updated(0, 0)(King).updated(1, 1)(Queen).representation shouldBe "K \n Q\n"
    Solution(3, 1).updated(1, 0)(Bishop).representation shouldBe " B \n"
  }

  "The solution" should "be able to create symmetrical solution" in {
    Solution(3, 3).updated(0, 0)(King).updated(2, 0)(King).updated(1, 2)(Rook).symmetric shouldBe
      Solution(3, 3).updated(2, 2)(King).updated(0, 2)(King).updated(1, 0)(Rook)
  }

}
